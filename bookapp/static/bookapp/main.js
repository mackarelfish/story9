(function () {
  console.log("hey");
})();

let hof = [];

async function handleClick(link) {
  let res = await fetch(link, { method: "GET" }).then((r) => r.json());
  let data = await res;
  hof.push(data.volumeInfo.title);
  var counts = {};
  hof.forEach(function (x) {
    counts[x] = (counts[x] || 0) + 1;
  });
  let items = Object.keys(counts);
  let html = "";
  let bookshof = document.querySelector(".books_hof");
  for (let i = 0; i < items.length; i++) {
    let template = `
      <div class="book">
        <p>${items[i]}</p>
        <p>${counts[items[i]]} likes</p>
      </div>
      `;
    html += template;
  }
  bookshof.innerHTML = html;
}

async function handleFetch(e) {
  let query = e.target.value;
  try {
    let res = await fetch(
      `https://www.googleapis.com/books/v1/volumes?q=${query}`,
      { method: "GET" }
    ).then((r) => r.json());
    let data = await res;

    let warn = document.querySelector("[data-test=warn]");
    warn.style.display = "none";
    let bookscontainer = document.querySelector(".books");

    let html = "";
    console.log(data);
    for (let i = 0; i < data.items.length; i++) {
      let bdata = data.items[i];
      let template = `
      <div class="book">
        <a href=${bdata.volumeInfo.previewLink}><p>${bdata.volumeInfo.title}</p></a>
        <button onclick="handleClick('${bdata.selfLink}')">Like</button>
      </div>
    `;
      html += template;
    }
    bookscontainer.innerHTML = html;
  } catch {
    let bookscontainer = document.querySelector(".books");
    bookscontainer.innerHTML = "";

    let warn = document.querySelector("[data-test=warn]");
    warn.style.display = "block";
  }
}
