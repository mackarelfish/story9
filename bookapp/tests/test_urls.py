from django.test import SimpleTestCase
from django.urls import reverse, resolve
from bookapp.views import index, search


class TestUrls(SimpleTestCase):

    def test_index_url_resolved(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)

    def test_search_url_resolved(self):
        url = reverse('search')
        self.assertEquals(resolve(url).func, search)
