from django.contrib.staticfiles.testing import LiveServerTestCase
from selenium import webdriver
import time
import os


class TestFunctional(LiveServerTestCase):

    @classmethod
    def setUpClass(self):
        super().setUpClass()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

    @classmethod
    def tearDownClass(self):
        self.browser.close()
        super().tearDownClass()

    def test_no_search_been_done(self):
        self.browser.get(self.live_server_url)
        warning = self.browser.find_element_by_css_selector(
            "h3[data-test='warn']")
        self.assertNotEqual(warning.value_of_css_property("display"), 'none')

    def test_search_working(self):
        self.browser.get(self.live_server_url)
        search = self.browser.find_element_by_css_selector(
            "input[data-test='search']")

        search.send_keys("andi")
        time.sleep(4)

        books = self.browser.find_elements_by_class_name("book")
        self.assertTrue(len(books) > 1)
