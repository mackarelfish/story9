from django.shortcuts import render


def index(request):
    return render(request, "bookapp/index.html")


def search(request):
    return render(request, "bookapp/search.html")
